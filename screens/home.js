import React from "react"
import {
	View,
	Text,
	TouchableNativeFeedback,
} from "react-native"
import { style } from "./style"
import AsyncStorage from "@react-native-community/async-storage"
import LinearGradient from "react-native-linear-gradient"

// const blue = ["#003251", "#00415e", "#005069", "#006072", "#006f79"]
// const red = ["#003251", "#00415e", "#005069", "#006072", "#006f79"]

class Contar extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			count: 0
		}
	}

	blue = ["#003251", "#00415e", "#005069", "#006072", "#006f79"]

	setData = async (storageKey, storedValue) => {
		try {
			await AsyncStorage.setItem(storageKey, storedValue)
		} catch (error) {
			// error
		}
	}

	getData = async (storageKey) => {
		let value = null
		try {
			value = await AsyncStorage.getItem(storageKey)
		} catch (error) {
			// error
		}
		return(value)
	}

	async componentDidMount() {
		let item = await this.getData("count")
		if (item == undefined || item == null) {
			item = "0"
		}
		this.setState({count: parseInt(item)})
		this.setData("count", String(this.state.count))
	}

	add = async () => {
		let item = this.state.count
		item++
		await this.setData("count", String(item))
		this.setState({count: item})
	}

	remove = async () => {
		let item = this.state.count
		item--
		await this.setData("count", String(item))
		this.setState({count: item})
	}

	render() {
		return (
			<View style={style.container}>

				<Text style={[style.Text, {marginBottom: 100}]}>{this.state.count}</Text>

				<TouchableNativeFeedback onPress={ async () => await this.add()}>
					<LinearGradient style={style.button} colors={this.blue}>
						<Text
							style={{
								fontFamily: "Manjari-Bold",
								fontSize: 60,
								marginHorizontal: 20,
								marginBottom: -18,
								marginTop: -14,
							}}
						>
						+
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>

				<TouchableNativeFeedback onPress={ async () => await this.remove()}>
					<LinearGradient style={[style.button, {marginBottom: -100}]} colors={this.blue}>
						<Text
							style={{
								fontFamily: "Manjari-Bold",
								fontSize: 60,
								marginHorizontal: 28,
								marginBottom: -23,
								marginTop: -14,
							} }
						>
						-
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>
		  </View>
		)
	}
}

export default Contar