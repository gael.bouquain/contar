import { StyleSheet } from "react-native";

export let background = "#202125"
export let textColor = "#007daa"

export const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: background,
        alignItems: "center",
        justifyContent: "center",
        fontFamily: "Manjari-Bold",
    },
    Text: {
        color: textColor,
        fontSize: 40,
        margin: 10,
        marginTop: 20,
        fontFamily: "Manjari-Bold",
        borderRadius: 200,
    },
    Symbole: {
        color: textColor,
        fontSize: 40,
        margin: 0,
        marginTop: -20,
        fontFamily: "Manjari-Bold",
    },
    text: {
        color: textColor,
        fontSize: 30,
        margin: 10,
        fontFamily: "Manjari-Bold",
    },
    Number: {
        color: "black",
        fontSize: 30,
        marginLeft: -45,
        marginTop: -7,
        fontFamily: "Manjari-Bold",
    },
    element: {
        width: 100,
        height: 100,
        borderWidth: 5,
        borderRadius: 10,
        borderColor: "black",
        alignItems: "center",
    },
    button: {
        margin: 25,
        padding: 0,
        borderRadius: 300,
        paddingHorizontal: 10,
        elevation: 2,
    }
})