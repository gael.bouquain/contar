import React from "react"
import { View, StatusBar } from "react-native"
import Contar from "./screens/home"

const App = () => {
	return (
		<View style={{ flex: 1 }}>
			<StatusBar hidden={true} />
			<Contar />
		</View>
	)
}

export default App
